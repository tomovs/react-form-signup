// Component for datepicker
var React = require('react');
var DatePicker = require("react-bootstrap-date-picker");
var Glyphicon = require('react-bootstrap/lib/Glyphicon');
var FormGroup = require('react-bootstrap/lib/FormGroup');
var ControlLabel = require('react-bootstrap/lib/ControlLabel');
var HelpBlock = require('react-bootstrap/lib/HelpBlock');
var Grid = require('react-bootstrap/lib/Grid');
var Row = require('react-bootstrap/lib/Row');
var Col = require('react-bootstrap/lib/Col');
var { Navbar, Button } = require('react-bootstrap');
var Nav = require('react-bootstrap/lib/Nav');
var NavItem = require('react-bootstrap/lib/NavItem');
var InputError = require('./InputError.js');

var DateApp = React.createClass({
  getInitialState() {
    return {
     
      previousDate: null,
      minDate: null,
      maxDate: null,
      focused: false
    };
  },
  handleChange(value) {
    this.setState({
      date: value
    });
  },
  handleMinChange(value) {
    this.setState({
      minDate: value
    });
  },
  handleMaxChange(value) {
    this.setState({
      maxDate: value
    });
  },
  handlePlacement() {
    return 'top';
  },
  handleRandomPlacement() {
    const placementKey = Math.floor((Math.random()*4) + 1);
    switch (placementKey) {
      case 1:
        return 'top';
      case 2:
        return 'left';
      case 4:
        return 'right';
      default:
        return 'bottom';
    }
  },
  render: function(){
    return <FormGroup controlId="change_handler">
            
            <DatePicker onChange={this.handleChange} placeholder="Date of Birth" value={this.state.date} id="change_handler_example" />
            <InputError 
          visible={this.state.errorVisible} 
          errorMessage={this.state.errorMessage} 
        />
          </FormGroup>
  }
});

module.exports = DateApp;