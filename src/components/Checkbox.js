// Component for checkbox input
var React = require('react');

var Checkbox = React.createClass ({
  render() {
    return (
      <fieldset className="input_label">
        <label
        	className="label_text"
          htmlFor={this.props.htmlFor}
          label={this.props.label}
        >
          <input
            id={this.props.htmlFor}
            name={this.props.name || null}
            required={this.props.required || null}
            type='checkbox'
          />
          {this.props.label}
        </label>
      </fieldset>
    );
  }
});

module.exports = Checkbox;