// Component for select input
var React = require('react');

var Select = React.createClass({
  render() {
    // Get all options from option prop
    const selectOptions = this.props.options.split(', ');

    // Generate list of options
    const selectOptionsList = selectOptions.map((selectOption, index) => {
      return <option className="Select-option" key={index} value={index}>{selectOption}</option>
    });

    return (
      <fieldset className="Select">
        <select
        	className="Select-control"
          defaultValue=''
          id={this.props.htmlFor}
          name={this.props.name || null}
          required={this.props.required || null}
        >
        	
          <option className="" value='' disabled>Country</option>

          {selectOptionsList}

        </select>
        <span className="Select-arrow" />
      </fieldset>
    );
  }
});

module.exports = Select;