// Component for radio input
var React = require('react');

var Radio = React.createClass ({
  render() {
    return (
      <fieldset className="input_label">
        <label
        	className="label_text"
          htmlFor={this.props.htmlFor}
          label={this.props.label}
        >
          <input
            id={this.props.htmlFor}
            name={this.props.name || null}
            required={this.props.required || null}
            type='radio'
          />
          {this.props.label}
        </label>
      </fieldset>
    );
  }
});

module.exports = Radio;