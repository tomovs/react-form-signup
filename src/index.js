// Render App component
require('./style.css')
require('babel-polyfill');
var React = require('react');
var ReactDOM = require('react-dom');
var App = require('./App.js');

ReactDOM.render(<App/>, document.getElementById('form'));