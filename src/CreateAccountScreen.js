// Component for creating account screen
var React = require('react');
var Input = require('./components/Input.js');
var _ = require('underscore');
var Select = require('./components/Select.js');
var Icon = require('./components/Icon.js');
var Radio = require('./components/Radio.js');
var Checkbox = require('./components/Checkbox.js');
var DatePicker = require('./components/DatePicker.js');

var CreateAccountScreen = React.createClass({
  getInitialState: function() {
    return {
    	firstLastName: null,
    	username: null,
      email: null,
      password: null,
      confirmPassword: null,
      date: null,
      phone: null,
      checkbox: null,
      forbiddenWords: ["password"]
    }
  },

  handlePasswordInput: function (event) {
    if(!_.isEmpty(this.state.confirmPassword)){
      this.refs.passwordConfirm.isValid();
    }
    this.refs.passwordConfirm.hideError();
    this.setState({
      password: event.target.value
    });
  },

  handleConfirmPasswordInput: function (event) {
    this.setState({
      confirmPassword: event.target.value
    });
  },

  saveAndContinue: function (e) {
    e.preventDefault();

    var canProceed = this.validateEmail(this.state.email) 
        
        && this.refs.password.isValid()
        && this.refs.passwordConfirm.isValid();

    if(canProceed) {
      var data = {
        email: this.state.email,
        firstLastName: this.state.firstLastName
      }
      alert('Thank You');
    } else {
      this.refs.firstLastName.isValid();
      this.refs.username.isValid();
      this.refs.email.isValid();
      this.refs.password.isValid();
      this.refs.passwordConfirm.isValid();
      this.refs.phone.isValid();
    }
  },

  isConfirmedPassword: function (event) {
    return (event == this.state.password)
  },

  handleNameInput: function(event) {
    this.setState({
      firstLastName: event.target.value
    })
  },

  validateName: function(event) {
  	var re = /[a-zA-Z0-9!#$%&'*+/=?^_`{|}~.-]{4,}/;
  	return re.test(event);
  },

  handleUsernameInput: function(event) {
    this.setState({
      username: event.target.value
    })
  },

  validateUsername: function (event) {
  	var re = /^[0-9a-zA-Z\_]{3,}$/;
  	return re.test(event);
  },

  handleEmailInput: function(event){
    this.setState({
      email: event.target.value
    });
  },

  validateEmail: function (event) {
    var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(event);
  },

  handlePhoneNumber: function(event) {
  	this.setState({
  		phone: event.target.value
  	});
  },

  validatePhone: function(event) {
  	var re = /^\d{8,}/;
  	return re.test(event);
  },

  handleDate: function(event) {
  	this.setState({
  		date: event.target.value
  	});
  },

  isEmpty: function (value) {
    return !_.isEmpty(value);
  },


  render: function() {
    return (
      <div className="create_account_screen">

        <div className="create_account_form">
          <h1>Register your account</h1>
          <p></p>
          <form onSubmit={this.saveAndContinue}>

          	<Input 
              text="First Name, Last Name" 
              ref="firstLastName"
              type="text"
              defaultValue={this.state.firstLastName}
              validate={this.validateName}
              value={this.state.firstLastName}
              onChange={this.handleNameInput}
              errorMessage="Your name must be longer" 
              emptyMessage="Please enter your name"
            />

            <Input 
              text="Username" 
              ref="username"
              type="text"
              defaultValue={this.state.username}
              validate={this.validateUsername}
              value={this.state.username}
              onChange={this.handleUsernameInput} 
              errorMessage="You can only use numbers, letters and _"
              emptyMessage="Username can't be empty"
            /> 

            <Input 
              text="E-mail" 
              ref="email"
              type="text"
              defaultValue={this.state.email} 
              validate={this.validateEmail}
              value={this.state.email}
              onChange={this.handleEmailInput} 
              errorMessage="Email is invalid"
              emptyMessage="Email can't be empty"
              errorVisible={this.state.showEmailError}
            />

            <Input 
              text="Password" 
              type="password"
              ref="password"
              validator="true"
              minCharacters="8"
              requireCapitals="1"
              requireNumbers="1"
              forbiddenWords={this.state.forbiddenWords}
              value={this.state.password}
              emptyMessage="Password is invalid"
              onChange={this.handlePasswordInput} 
            /> 

            <Input 
              text="Confirm password" 
              ref="passwordConfirm"
              type="password"
              validate={this.isConfirmedPassword}
              value={this.state.confirmPassword}
              onChange={this.handleConfirmPasswordInput} 
              emptyMessage="Please confirm your password"
              errorMessage="Passwords don't match"
            /> 

            <DatePicker />

						<Input 
              text="Phone number" 
              ref="phone"
              type="text"
              defaultValue={this.state.phone}
              validate={this.validatePhone}
              value={this.state.phone}
              onChange={this.handlePhoneInput} 
              errorMessage="You can only use numbers. Must be longer than 8 numbers"
              emptyMessage="Phone number can't be empty"
            />        

            <h1>Gender:</h1>
            <Radio
          		hasLabel='true'
          		htmlFor='male'
          		label='Male'
          		name='radio'
       		 	/>    

       		 	<Radio
          		hasLabel='true'
          		htmlFor='female'
          		label='Female'
          		name='radio'
       		 	/>    

       		 	<Select
		          hasLabel='true'
		          htmlFor='country'
		          label='Country'
		          options='USA, France, Italy, England, Ukraine'
		        />

		        <Checkbox
		          hasLabel='true'
		          htmlFor='checkbox'
		          label='Registration agreement'
		          errorMessage=""
              emptyMessage=""
		        />

            <button 
              type="submit" 
              className="button button_wide">
              REGISTER ACCOUNT
            </button>

          </form>

        </div>
      </div>
    );
  }   
});
    
module.exports = CreateAccountScreen;